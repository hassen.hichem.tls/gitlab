export const REQUEST_PROTECTED_ENVIRONMENTS = 'REQUEST_PROTECTED_ENVIRONMENTS';
export const RECEIVE_PROTECTED_ENVIRONMENTS_SUCCESS = 'RECEIVE_PROTECTED_ENVIRONMENTS_SUCCESS';
export const RECEIVE_PROTECTED_ENVIRONMENTS_ERROR = 'RECEIVE_PROTECTED_ENVIRONMENTS_ERROR';

export const REQUEST_MEMBERS = 'REQUEST_MEMBERS';
export const RECEIVE_MEMBERS_FINISH = 'RECEIVE_MEMBERS_SUCCESS';
export const RECEIVE_MEMBERS_ERROR = 'RECEIVE_MEMBERS_ERROR';

export const RECEIVE_MEMBER_SUCCESS = 'RECEIVE_MEMBER_SUCCESS';

export const REQUEST_UPDATE_PROTECTED_ENVIRONMENT = 'REQUEST_UPDATE_PROTECTED_ENVIRONMENT';
export const RECEIVE_UPDATE_PROTECTED_ENVIRONMENT_SUCCESS =
  'RECEIVE_UPDATE_PROTECTED_ENVIRONMENT_SUCCESS';
export const RECEIVE_UPDATE_PROTECTED_ENVIRONMENT_ERROR =
  'RECEIVE_UPDATE_PROTECTED_ENVIRONMENT_ERROR';
